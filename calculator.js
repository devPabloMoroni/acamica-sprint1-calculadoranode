const fs=require('fs');

function creaTxt(){
    fs.writeFile('./log.txt', "", error => {
    if (error)
        console.log(error);
    else
        console.log('El archivo fue creado');
    });
}

function suma(num1,num2){
    return num1 + num2;
}

function resta(num1,num2){
    return num1 - num2;
}

function multi(num1,num2){
    return num1 * num2;
}

function divi(num1,num2){
    return num1 / num2; 
}

module.exports = {
    suma,
    resta,
    multi,
    divi,
    creaTxt
}