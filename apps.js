const calculadora = require('./calculator'); //Importo el modulo calculadora
const fs=require('fs'); //Importo el modulo FS para el manejo de archivos

calculadora.creaTxt(); //Ejecuto la funcion crearTxt dentro de calulator para crear el txt

let num1 = 5;
let num2 = 4;

resSuma = calculadora.suma(num1,num2); //Guardo el resultado de la suma en resSuma 
let txtSum = num1 + " + " + num2 + " = " + resSuma + "\n"; //Agrego las operaciones a una variable txt
fs.appendFile('./log.txt', txtSum, error => { //paso el texto de la variable sum como parametro para ser añadido
    if (error)
    console.log(error);
    else
    console.log('La suma se agregó exitosamente');
});

//Misma logica para el resto de las operaciones
resResta = calculadora.resta(num1,num2);
let txtRes = num1 + " - " + num2 + " = " + resResta + "\n";
fs.appendFile('./log.txt', txtRes, error => {
    if (error)
    console.log(error);
    else
    console.log('La resta se agregó exitosamente');
});

resMulti = calculadora.multi(num1,num2);
let txtMulti = num1 + " * " + num2 + " = " + resMulti + "\n";
fs.appendFile('./log.txt', txtMulti, error => {
    if (error)
    console.log(error);
    else
    console.log('la multiplicacion se agregó exitosamente');
});

if(num2 != 0){
    resDivi = calculadora.divi(num1,num2);
    let txtDivi = num1 + " / " + num2 + " = " + resDivi + "\n";
    fs.appendFile('./log.txt', txtDivi, error => {
        if (error)
        console.log(error);
        else
        console.log('La division se agregó exitosamente');
    });
}
else{
    let txtDivi = "La division por 0 no esta permitida!\n";
    fs.appendFile('./log.txt', txtDivi, error => {
        if (error)
        console.log(error);
        else
        console.log('La division por 0 no esta permitida!');
    });
}
